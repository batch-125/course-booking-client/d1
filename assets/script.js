//DOM Manipulation

let profile = document.querySelector('#profile');
let navSession = document.querySelector('#navSession');
let registerLink = document.querySelector('#register');
let userToken = localStorage.getItem('token');
let admin = localStorage.getItem('isAdmin');

if(!userToken){

	navSession.innerHTML = 
	`
		<li class="nav-item">
			<a href="./login.html" class="nav-link">Login</a>
		</li>
	`

	registerLink.innerHTML =
	`
		<li class="nav-item">
			<a href="./register.html" class="nav-link">Register</a>
		</li>
	`
} else {

	if(admin === "false"){
		profile.innerHTML =
		`
			<li class="nav-item">
				<a href="./profile.html" class="nav-link">Profile</a>
			</li>
		`
		navSession.innerHTML =
		`
			<li class="nav-item">
				<a href="./logout.html" class="nav-link">Logout</a>
			</li>
		`
	} else {

		navSession.innerHTML =
		`
			<li class="nav-item">
				<a href="./logout.html" class="nav-link">Logout</a>
			</li>
		`
	}

}