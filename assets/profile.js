
let token = localStorage.getItem('token')

let name = document.querySelector('#userName');
let email = document.querySelector('#email');
let mobileNo = document.querySelector('#mobileNo')
let editButton = document.querySelector('#editButton')
let enrollContainer = document.querySelector(`#enrollContainer`)

fetch('http://localhost:3000/api/users/details',
	{
		method: "GET",
		headers: {
			"Authorization": `Bearer ${token}`
		}
	}
)
.then(result => result.json())
.then( result => {
	console.log(result)


		// to display user details in the web page
		name.innerHTML = `${result.firstName} ${result.lastName}`
		email.innerHTML = result.email
		mobileNo.innerHTML = result.mobileNo
		editButton.innerHTML =
		`
			<div class="mb-2">
				<a href="./editProfile.html" class="btn btn-primary">Edit Profile</a>
			</div>
		`
		result.enrollments.forEach((course)=> {
			console.log(course)

			let courseId = course.courseId

			fetch(`http://localhost:3000/api/courses/${courseId}`,
				{
					method: "GET",
					headers: {
						"Authorization": `Bearer ${token}`
					}	
				}

			)
			.then(result => result.json())
			.then(result => {
				console.log(result)

				if(result){
					enrollContainer.innerHTML = 
					`
					<div class="card my-5">
						<h4 class="card-title">${result.name}</h4>
						<p class="card-title">${result.description}</p>
						<h5 class="card-title">${course.enrolledOn}</h5>
						<p class="card-title">${course.enrolledOn}</p>
					</div>
					`
				} else {
					alert(`Something Went Wrong`)
				}
			})
		})


})