//target the form
let createCourse = document.querySelector('#createCourse');

//listen to an even (such as submit)
createCourse.addEventListener("submit", (e) => {
	e.preventDefault();

//get the values of the form
let courseName = document.querySelector('#courseName').value
let coursePrice = document.querySelector('#coursePrice').value
let courseDesc = document.querySelector('#courseDesc').value

	if(courseName !== "" && coursePrice !== "" && courseDesc !== ""){
	//use fetch to send the data to the server

		let token = localStorage.getItem("token");

		fetch("http://localhost:3000/api/courses/addCourse", 
			{
				method: "POST",
				headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${token}`
				},
				body: JSON.stringify({
					name: courseName,
					price: coursePrice,
					description: courseDesc
				})
			}
		)
		.then( result => result.json())
		.then( result => {
			// console.log(result)

			if(result){
				alert("Course Succesfully Created");

				window.location.replace('./courses.html')
			} else {
				alert("Course Creation Failed. Something went wrong.")
			}

		})
	}

})
//when response is received, alert "Course Succesfully Created", else alert "Course Creation Failed. Something went wrong"

//if success, redirect back to courses.html
